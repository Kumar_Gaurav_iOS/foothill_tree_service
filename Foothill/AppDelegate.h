//
//  AppDelegate.h
//  Foothill
//
//  Created by coding Brains on 07/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

