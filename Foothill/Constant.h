//
//  Constant.h
//  MyCoffee
//
//  Created by coding Brains on 05/07/18.
//  Copyright © 2018 KUMAR GAURAV. All rights reserved.
//

#ifndef Constant_h
#define Constant_h
#import "AppDelegate.h"
#import <MBProgressHUD.h>
#import "LoginVC.h"
#import "KSToastView.h"
#import "Reachability.h"
#import "DEMORootViewController.h"
#import "URLConfiguration.h"
#import "Records.h"
#import "EstimatorDetails.h"
#import "Estimator_Details.h"
//#import "Estimate_ImageData.h"
#import "Estimate_TreeDetails.h"
#import "Estimate_Master.h"
#import "OMW_CollectionViewCell.h"
#import "EstimateVC.h"
#import "ViewByFeildTableViewCell.h"
#import "ExportTableViewCell.h"
#import "EstimatorTableViewCell.h"


//#import "Estimate_PropertyData.h"
//#import "Estimator_Master.h"


#define BaseUrl @"http://52.40.174.179/FootHill/Api/"// LIVE
#define APIKey @"YYwPiC4z4jcdRwq8fDduRLVFdRYXeyCETWsWQIQXgOoShugz6crvggDob0cAkwJnksWdAFynJ47JAyCkvNB7pKNWYsP0quiWdUnugneJnsZFHaOOhIFvYL0I2v4YPIHJBzztfOIBMYzWugIlTN6ybgsqpF2z7m8EooCtwe1ya64riOL6YmIgikRjld5wTaMq8qhl540XfsykOfaEWhgPrTX5gsH95FSuz7xA52wxwn9Hjixw8VmpJjPqkAmTqBPZ"// LIVE

//apiKey" : "YYwPiC4z4jcdRwq8fDduRLVFdRYXeyCETWsWQIQXgOoShugz6crvggDob0cAkwJnksWdAFynJ47JAyCkvNB7pKNWYsP0quiWdUnugneJnsZFHaOOhIFvYL0I2v4YPIHJBzztfOIBMYzWugIlTN6ybgsqpF2z7m8EooCtwe1ya64riOL6YmIgikRjld5wTaMq8qhl540XfsykOfaEWhgPrTX5gsH95FSuz7xA52wxwn9Hjixw8VmpJjPqkAmTqBPZ
//http://mycofffebond-env.asgxxmfjia.us-east-2.elasticbeanstalk.com/login

#define Crew @"Crew"
#define Estimator @"Estimator"
#define Foreman @"Foreman"
#define isLogin_Estimator @"isLogin_Estimator"
#define isLogin_Crew @"isLogin_Crew"
#define isLogin_Foreman @"isLogin_Foreman"

//----------- TABLEVIEW CELL -------------
#define CELL_VIEW_BY_FIELD @"viewByFeild"
#define CELL_ESTIAMTE @"Estimate_cell"
#define CELL_EXPORT @"export"



#define appDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

#endif /* Constant_h */
// Info Details

//MBprogressHUD
//[MBProgressHUD showHUDAddedTo:self.view animated:YES];//---> Show
//[MBProgressHUD hideHUDForView:self.view animated:YES];//---> Hide
