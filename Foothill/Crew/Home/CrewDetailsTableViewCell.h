//
//  CrewDetailsTableViewCell.h
//  Foothill
//
//  Created by coding Brains on 13/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CrewDetailsTableViewCell : UITableViewCell

@end

NS_ASSUME_NONNULL_END
