//
//  Crew_HomeVC.m
//  Foothill
//
//  Created by coding Brains on 08/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import "Crew_HomeVC.h"
#import "Constant.h"


@interface Crew_HomeVC ()
{
    NSDictionary *dictForCrewDetails;

}
@end

@implementation Crew_HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    
}




#pragma mark - API'S
#pragma mark -
-(void)getCrewDetailsAPI{
    
    @try{
        
        if([[URLConfiguration sharedInstance] isInternetAvailable])
        {
            [KSToastView ks_showToast:@"Internet connectivity issue" delay:0.1f];
            return;
        }
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSMutableDictionary *params =[[NSMutableDictionary alloc]init];
        
       
        [params setObject:@"214870c2-01f1-4945-b11a-6fbb6b947a7d" forKey:@"GUID"];
        [params setObject:@"YYwPiC4z4jcdRwq8fDduRLVFdRYXeyCETWsWQIQXgOoShugz6crvggDob0cAkwJnksWdAFynJ47JAyCkvNB7pKNWYsP0quiWdUnugneJnsZFHaOOhIFvYL0I2v4YPIHJBzztfOIBMYzWugIlTN6ybgsqpF2z7m8EooCtwe1ya64riOL6YmIgikRjld5wTaMq8qhl540XfsykOfaEWhgPrTX5gsH95FSuz7xA52wxwn9Hjixw8VmpJjPqkAmTqBPZ" forKey:@"apiKey"];
        [params setObject:@"fe599778-5e0b-47a5-8458-5111cb6e3cdb" forKey:@"RollId"];
        
        NSLog(@"%@",params);
        NSMutableString* parameterString = [NSMutableString string];
        for(NSString* key in [params allKeys])
        {
            if ([parameterString length]) {
                
                [parameterString appendString:@"&"];
            }
            [parameterString appendFormat:@"%@=%@",key, params[key]];
        }
        NSString* urlString = [NSString stringWithFormat:@"%@GetCrewJobDetails",BaseUrl];
        NSURL* url = [NSURL URLWithString:urlString];
        
        NSURLSession* session =[NSURLSession sharedSession];
        NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[parameterString dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPShouldHandleCookies:NO];
        
        NSURLSessionDataTask* task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(error)
            {
                //do something
                NSLog(@"%@", error);
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSError *myError = nil;
                    
                    NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                    NSLog(@"%@",requestReply);
                    NSData *data = [requestReply dataUsingEncoding:NSUTF8StringEncoding];
                    NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                                 options:kNilOptions
                                                                                   error:&myError];
                    NSMutableDictionary*dic_response=[[NSMutableDictionary alloc]init];
                    
                    NSLog(@"%@",jsonResponse);
                    NSLog(@"%@",error);
                    
                    // NSURLResponse* response = // the response, from somewhere
                    
                    
                    //-----------* Get Header data from response *-----------
                    NSDictionary * headers = [(NSHTTPURLResponse *)response allHeaderFields];
                    //-------------------------------------------------------
                    //                       -------
                    //-------------* Get status from response *--------------
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                    long response_status = (long)[httpResponse statusCode];
                    NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
                    //-------------------------------------------------------
                    
                    // now query `headers` for the header you want
                    
                    
                    //------------ Response Code --------------
                    //200 --> succuess
                    //202,400 or other --> Failed in response
                    if(response_status == 200) {
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        self->dictForCrewDetails = [jsonResponse objectForKey:@"Data"];
                        
          
                        //RollId
//                        [[NSUserDefaults standardUserDefaults] setValue:[self->dictLogin objectForKey:@"RollId"]  forKey:@"RollId"];
//
//                        //Name
//                        [[NSUserDefaults standardUserDefaults] setValue:self->str_memberType  forKey:@"Name"];
     
                        NSLog(@"RollId %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"RollId"]);
                        NSLog(@"ImagePath %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"ImagePath"]);
                        NSLog(@"FullName %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"FullName"]);
                        
                
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLogin"];
                        //usersCredit
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        NSLog([[NSUserDefaults standardUserDefaults]boolForKey:@"isLogin"]?@"YES":@"NO");
                        
                        NSLog(@"%@",dic_response);
                        
                        //------------- Once Executed code ----------------
                        static dispatch_once_t once;
                        dispatch_once(&once, ^ {
                            NSLog(@"Do it once");
                            
                            
                        });
                        
                    }
                    else
                    {
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        
                        if ([[jsonResponse objectForKey:@"flag"] isEqualToString:@"unsuccess"]) {
                            UIAlertController * alert=   [UIAlertController
                                                          alertControllerWithTitle:@"Alert"
                                                          message:@"Error to like!"
                                                          preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* yesButton = [UIAlertAction
                                                        actionWithTitle:@"ok"
                                                        style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                                        {
                                                            //Handel your yes please button action here
                                                        }];
                            
                            [alert addAction:yesButton];
                            [self presentViewController:alert animated:YES completion:nil];
                        }
                        
                        
                    }
                    
                });
            }
        }];
        [task resume];
    }//try
    
    @catch (NSException *exception) {
        NSLog(@"exception at btn_Recordings_like_clicked :%@",exception);
    }
    @finally{
        
    }
}

@end
