//
//  EditTableViewCell.h
//  Foothill
//
//  Created by coding Brains on 08/03/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EditTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl_username;
@property (strong, nonatomic) IBOutlet UIView *view_edit;
@property (strong, nonatomic) IBOutlet UIButton *btn_profile;

@end

NS_ASSUME_NONNULL_END
