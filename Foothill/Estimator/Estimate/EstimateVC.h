//
//  EstimateVC.h
//  Foothill
//
//  Created by coding Brains on 04/03/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EstimateVC : UIViewController
@property (strong, nonatomic) IBOutlet UIView *view_collectionViewContainer;
@property (strong, nonatomic) IBOutlet UICollectionView *colView_OMW;
@property (strong, nonatomic) IBOutlet UILabel *lbl_status;
@property (strong, nonatomic) IBOutlet UIView *view_status;
@property (strong, nonatomic) IBOutlet UITableView *tblView_Estimator;
@property (strong, nonatomic) IBOutlet MKMapView *map_customerAddress;


- (IBAction)btn_backAction:(id)sender;



@end

NS_ASSUME_NONNULL_END
