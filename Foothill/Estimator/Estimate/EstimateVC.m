//
//  EstimateVC.m
//  Foothill
//
//  Created by coding Brains on 04/03/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import "EstimateVC.h"
#import "Constant.h"
@interface EstimateVC ()
{
    NSArray *arrOMW;
    NSMutableArray *arrStatus_OnMyWayM;

}
@end

@implementation EstimateVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self intializesAllVariables];

}

-(void)intializesAllVariables{
    // do initializes
    arrStatus_OnMyWayM = [[NSMutableArray alloc]initWithObjects:@NO,@NO,@NO, nil];
    arrOMW = [[NSArray alloc]initWithObjects:@"OMW",@"Start",@"Finish",@"Send",@"Approval",@"Copy to job", nil];

}

- (IBAction)zoomToCurrentLocation:(UIBarButtonItem *)sender {
    float spanX = 0.00725;
    float spanY = 0.00725;
    MKCoordinateRegion region;
    region.center.latitude = self.map_customerAddress.userLocation.coordinate.latitude;
    region.center.longitude = self.map_customerAddress.userLocation.coordinate.longitude;
    region.span.latitudeDelta = spanX;
    region.span.longitudeDelta = spanY;
}


#pragma mark- CollectionView Delegate and Datasource method
#pragma mark-

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    OMW_CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"OMW" forIndexPath:indexPath];
    if (cell == nil) {
        
    }
    //NSLog(@"%@",arrOMW[indexPath.row]);
    
    
    return cell;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return  arrOMW.count;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    
    return 0.0;
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    
    return 0.0;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:NO];
    
    
}
#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return arrOMW.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"LeftMenuCell";
    
    EstimatorTableViewCell *cell = [_tblView_Estimator dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[EstimatorTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
//    cell.l.text = arrMenuItem[indexPath.row];
    [cell.textLabel setText:[NSString stringWithFormat:@"%@",arrOMW[indexPath.row]]];
    return cell;
}

- (IBAction)btn_backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
