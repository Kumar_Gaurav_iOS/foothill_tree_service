//
//  EstimateTableViewCell.h
//  Foothill
//
//  Created by coding Brains on 19/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EstimateTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl_estimateName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_sr;
@property (strong, nonatomic) IBOutlet UILabel *lbl_estimate_id;
@property (strong, nonatomic) IBOutlet UILabel *lbl_customer_name;
@property (strong, nonatomic) IBOutlet UILabel *lbl_customerAddress;
@property (strong, nonatomic) IBOutlet UILabel *lbl_totalAmt;
@property (strong, nonatomic) IBOutlet UILabel *lbl_status;
@property (strong, nonatomic) IBOutlet UILabel *lbl_enable;
@property (strong, nonatomic) IBOutlet UILabel *lbl_sr_value;
@property (strong, nonatomic) IBOutlet UILabel *lbl_estimateId_value;
@property (strong, nonatomic) IBOutlet UILabel *lbl_customerName_value;
@property (strong, nonatomic) IBOutlet UILabel *lbl_salesPerson_value;
@property (strong, nonatomic) IBOutlet UILabel *lbl_customerAdd_value;
@property (strong, nonatomic) IBOutlet UILabel *lbl_totalAmt_value;
@property (strong, nonatomic) IBOutlet UILabel *lbl_status_value;
@property (strong, nonatomic) IBOutlet UILabel *lbl_enable_value;

@property (strong, nonatomic) IBOutlet UILabel *lbl_salaePerson;

//---------------- Outlet for Constraints -------------
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cons_1;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cons_2;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cons_3;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cons_4;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cons_5;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cons_6;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cons_7;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cons_8;

//----------- Outlet Constraints For those Value ---------
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cons_11;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cons_22;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cons_33;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cons_44;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cons_55;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cons_66;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cons_77;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cons_88;


@end

NS_ASSUME_NONNULL_END
