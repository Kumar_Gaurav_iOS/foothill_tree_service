//
//  ExportTableViewCell.h
//  Foothill
//
//  Created by coding Brains on 07/03/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ExportTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl_exportType;

@end

NS_ASSUME_NONNULL_END
