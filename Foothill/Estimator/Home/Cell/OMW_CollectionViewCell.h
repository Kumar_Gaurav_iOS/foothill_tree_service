//
//  OMW_CollectionViewCell.h
//  Foothill
//
//  Created by coding Brains on 04/03/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OMW_CollectionViewCell : UICollectionViewCell

@end

NS_ASSUME_NONNULL_END
