//
//  ViewByFeildTableViewCell.h
//  Foothill
//
//  Created by coding Brains on 06/03/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ViewByFeildTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *btn_checked;
@property (strong, nonatomic) IBOutlet UILabel *lbl_feildValue;

@end

NS_ASSUME_NONNULL_END
