//
//  Estimator_HomeVC.h
//  Foothill
//
//  Created by coding Brains on 08/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Estimator_HomeVC : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tbl_estimate;
@property (strong, nonatomic) IBOutlet UIView *view_search;
@property (strong, nonatomic) IBOutlet UIView *view_below_navigation;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UIButton *btn_cancelSearch;
- (IBAction)btn_Action_OnCancelSearch:(id)sender;
- (IBAction)btn_ActionOnSearch:(id)sender;
- (IBAction)action_viewByField:(id)sender;
- (IBAction)action_exportClicked:(id)sender;
- (IBAction)action_expandClicked:(id)sender;

//@property (strong, nonatomic) IBOutlet UIButton *action_expandClicked;

//---------------- Alert View with checked item ---------------
@property (strong, nonatomic) IBOutlet UIView *view_alert;
@property (strong, nonatomic) IBOutlet UITableView *tblView_byFeild;
//---------------- Export View ---------------
@property (strong, nonatomic) IBOutlet UIView *view_export;
@property (strong, nonatomic) IBOutlet UITableView *tblView_export;
//------------------ Outlet for Constraints ---------------------
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cons_tblView_height;


@end

NS_ASSUME_NONNULL_END
