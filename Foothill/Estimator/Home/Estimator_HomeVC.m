//
//  Estimator_HomeVC.m
//  Foothill
//
//  Created by coding Brains on 08/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import "Estimator_HomeVC.h"
#import "EstimateTableViewCell.h"
#import "Constant.h"
#define HEIGHT 20

@interface Estimator_HomeVC ()<UISearchBarDelegate>
{
    NSArray *arr_estimates,*arr_byFeildContent,*arr_searchContent,*arr_export;
    NSMutableDictionary* dicEstimateDetailsM,*dicFetchDetails;
    NSMutableArray *arr,*arrGetDetailsM,*arr_byFieldM,*arrDemoM;
    BOOL toggle_ViewByField,isSearch,toggle_export,toggle_expand;
    NSInteger rowHeight;

}
@end

@implementation Estimator_HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self intializesAllVariables];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    
    rowHeight = 200;

//        [self getEstimatorDetailsAPI];

}


#pragma mark- CUSTOM METHOD
#pragma mark-
-(void)intializesAllVariables{
    // do initializes
    
    arrGetDetailsM = [[NSMutableArray alloc]init];
    
    _view_search.hidden = YES;
    _view_below_navigation.hidden = NO;
    arrDemoM = [[NSMutableArray alloc]init];
    
    arr_byFeildContent = [[NSArray alloc]initWithObjects:@"Sr. no.",@"Estimate Id",@"Customer Name",@"Sales Person",@"Customer Address",@"Total Amount($)",@"Status",@"Disable/Enable", nil];

    arr_export = [[NSArray alloc]initWithObjects:@"CSV",@"PDF",nil];
    
    //------- All toggle Value set to NO ---------
    toggle_ViewByField = NO;
    toggle_export = NO;
    toggle_expand = NO;
    isSearch = NO;

    //------- All Other View initial set Hidden ---------
    _view_alert.hidden = YES;
    _view_export.hidden = YES;

    arr_byFieldM  = [[NSMutableArray alloc]init];
    NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];

    for (int i = 0; i< arr_byFeildContent.count; i++) {
        [arr_byFieldM setObject:@"1" atIndexedSubscript:i];

    }
    
    [dic setValue:@"gaurav" forKey:@"name"];
    [arrDemoM addObject:dic];
    
    dic = [[NSMutableDictionary alloc]init];
    
    [dic setValue:@"Ankur" forKey:@"name"];
    [arrDemoM addObject:dic];
    
    dic = [[NSMutableDictionary alloc]init];

    [dic setValue:@"Sachin" forKey:@"name"];
    [arrDemoM addObject:dic];
    
    dic = [[NSMutableDictionary alloc]init];
    
    [dic setValue:@"Rahul" forKey:@"name"];
    [arrDemoM addObject:dic];

    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hide_AlertView:)];
    [_view_alert addGestureRecognizer:tapRecognizer];

    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hide_ExportView:)];
    [_view_export addGestureRecognizer:tapRecognizer];
    

}

-(void)hide_AlertView:(UIButton *) sender {
    _view_alert.hidden = YES;
    
}


-(void)hide_ExportView:(UIButton *) sender {
    
    _view_export.hidden = YES;
    
}

-(void)btn_CheckedAction:(UIButton *) sender {
    
      ViewByFeildTableViewCell *my_cell = [_tblView_byFeild cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];

    
            if ([[arr_byFieldM objectAtIndex:sender.tag] isEqualToString:@"1"]) {
                [arr_byFieldM setObject:@"0" atIndexedSubscript:sender.tag];
                UIImage *img = [UIImage imageNamed:@"unchecked"];
                [my_cell.btn_checked setImage:img forState:UIControlStateNormal];
                rowHeight -= HEIGHT;
                
            }
            else{
                [arr_byFieldM setObject:@"1" atIndexedSubscript:sender.tag];
                UIImage *img = [UIImage imageNamed:@"checked"];
                [my_cell.btn_checked setImage:img forState:UIControlStateNormal];
                rowHeight += HEIGHT;

            }
    
    [_tbl_estimate reloadData];
    
}



-(void)garbageData{
    
    dicEstimateDetailsM = [[NSMutableDictionary alloc]init];
    [dicEstimateDetailsM setValue:@"Don" forKey:@"name"];
    [dicEstimateDetailsM setValue:@"1" forKey:@"id"];
    [dicEstimateDetailsM setValue:@"922123313" forKey:@"contact"];
    
    //    [[URLConfiguration sharedInstance]insert_AllData_DB_FromServer_Details:dicEstimateDetailsM];
    dicFetchDetails = [[NSMutableDictionary alloc]init];
    arr = [[NSMutableArray alloc]init];
    arr = [[URLConfiguration sharedInstance]FetchDataFromDataBase];
    
    NSManagedObject *obj=[arr objectAtIndex:0];
    [dicFetchDetails setValue:[obj valueForKey:@"name"] forKey:@"name"];
    [dicFetchDetails setValue:[obj valueForKey:@"id"] forKey:@"id"];
    [dicFetchDetails setValue:[obj valueForKey:@"contact"] forKey:@"contact"];
    
    NSLog(@"Value of get Details %@",dicFetchDetails);
}

#pragma mark - API'S
#pragma mark -
-(void)getEstimatorDetailsAPI{
    
    @try{
        
        if([[URLConfiguration sharedInstance] isInternetAvailable])
        {
            [KSToastView ks_showToast:@"Internet connectivity issue" delay:0.1f];
            return;
        }
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSMutableDictionary *params =[[NSMutableDictionary alloc]init];
      
        [params setObject:[[NSUserDefaults standardUserDefaults]valueForKey:@"RollId"] forKey:@"RollId"];
        [params setObject:APIKey forKey:@"apiKey"];
        [params setObject:[[NSUserDefaults standardUserDefaults]valueForKey:@"Id"] forKey:@"GUID"];
        
        NSLog(@"%@",params);
        NSMutableString* parameterString = [NSMutableString string];
        for(NSString* key in [params allKeys])
        {
            if ([parameterString length]) {
                
                [parameterString appendString:@"&"];
            }
            [parameterString appendFormat:@"%@=%@",key, params[key]];
        }
        
        NSString* urlString = [NSString stringWithFormat:@"%@GetEstimateData",BaseUrl];
        NSURL* url = [NSURL URLWithString:urlString];
        
        NSURLSession* session =[NSURLSession sharedSession];
        NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPBody:[parameterString dataUsingEncoding:NSUTF8StringEncoding]];

        [request setHTTPMethod:@"POST"];
        [request setHTTPShouldHandleCookies:NO];
        
        NSURLSessionDataTask* task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(error)
            {
                //do something
                NSLog(@"%@", error);
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSError *myError = nil;
                    
                    NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                    NSLog(@"%@",requestReply);
                    NSData *data = [requestReply dataUsingEncoding:NSUTF8StringEncoding];
                    NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                                 options:kNilOptions
                                                                                   error:&myError];
                    NSMutableDictionary*dic_response=[[NSMutableDictionary alloc]init];
                    
                    NSLog(@"%@",jsonResponse);
                    NSLog(@"%@",error);
                    
                    // NSURLResponse* response = // the response, from somewhere
                    
                    
                    //-----------* Get Header data from response *-----------
                    NSDictionary * headers = [(NSHTTPURLResponse *)response allHeaderFields];
                    //-------------------------------------------------------
                    //                       -------
                    //-------------* Get status from response *--------------
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                    long response_status = (long)[httpResponse statusCode];
                    NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
                    //-------------------------------------------------------
                    
                    // now query `headers` for the header you want
                    
                    
                    //------------ Response Code --------------
                    //200 --> succuess
                    //202,400 or other --> Failed in response
                    if(response_status == 200) {
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        self->arr_estimates = jsonResponse;
                        
                        if (self->arr_estimates.count > 0) {
//                            [[EstimatorDetails sharedInstance]insert_AllData_DB_FromServer_Details:self->arr_estimates];
                        }
             
                        
//                        arrGetDetailsM = [[EstimatorDetails sharedInstance]FetchDataFromDataBase];
                        
                        for (int i = 0; i< arrGetDetailsM.count; i++) {
                            NSManagedObject *obj=[arrGetDetailsM objectAtIndex:i];
//                            NSLog(@"%@",[obj valueForKey:@"tree_id"]);
                            
                        }
                        

                        [self.tbl_estimate reloadData];
                        
                        
                        //------------- Once Executed code ----------------
                        static dispatch_once_t once;
//                        dispatch_once(&once, ^ {
//                            NSLog(@"Do it once");
//                            
//                            
//                        });
                        
                    }
                    else
                    {
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        
                        if ([[jsonResponse objectForKey:@"flag"] isEqualToString:@"unsuccess"]) {
                            UIAlertController * alert=   [UIAlertController
                                                          alertControllerWithTitle:@"Alert"
                                                          message:@"Error to like!"
                                                          preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* yesButton = [UIAlertAction
                                                        actionWithTitle:@"ok"
                                                        style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                                        {
                                                            //Handel your yes please button action here
                                                        }];
                            
                            [alert addAction:yesButton];
                            [self presentViewController:alert animated:YES completion:nil];
                        }
                        
                        
                    }
                    
                });
            }
        }];
        [task resume];
    }//try
    
    @catch (NSException *exception) {
        NSLog(@"exception at btn_Recordings_like_clicked :%@",exception);
    }
    @finally{
        
    }
}



#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString * storyboardName = @"Estimator";
    UIStoryboard *storyboard;
    
    storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    
    EstimateVC * vc = [storyboard instantiateViewControllerWithIdentifier:@"EstimateVC"];
    [self.navigationController pushViewController:vc animated:YES];
       // [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tbl_estimate) {
        if (rowHeight < 200) {
            return rowHeight;
        }
        
        else{
            return 200;
        }
    }
   else if (tableView == _tblView_export) {
       return 40;

    }
    else{
        return 44;
    }
   
}

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    if (tableView == _tbl_estimate) {
        if (isSearch) {
            return [arr_searchContent count];
        }
        else{
            return arrDemoM.count;
        }
        
    }
    else if(tableView == _tblView_export){
        
        return arr_export.count;
    }
    else{
        return arr_byFeildContent.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    

    
    if (tableView == _tbl_estimate) {
    
    EstimateTableViewCell *cell = [_tbl_estimate dequeueReusableCellWithIdentifier:CELL_ESTIAMTE];
    if (cell == nil) {
        cell = [[EstimateTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CELL_ESTIAMTE];
    }
    
    NSString *str_estimate;
    str_estimate = [NSString stringWithFormat:@"Estimate %ld",indexPath.row+1];
    cell.lbl_sr_value.text = str_estimate;
    cell.lbl_enable_value.text = str_estimate;
    cell.lbl_status_value.text = str_estimate;
    cell.lbl_totalAmt_value.text = str_estimate;
    cell.lbl_customerAdd_value.text = str_estimate;
    cell.lbl_customerName_value.text = str_estimate;
    cell.lbl_totalAmt_value.text = str_estimate;
    cell.lbl_estimateId_value.text = str_estimate;
    cell.lbl_salesPerson_value.text = str_estimate;

    //-------------- Add or Remove Sr. no. ---------------
    if ([[arr_byFieldM objectAtIndex:0] isEqualToString:@"1"]) {
        [cell.lbl_sr_value setHidden:NO];
        [cell.lbl_sr setHidden:NO];

        cell.cons_1.constant = HEIGHT;
        cell.cons_11.constant = HEIGHT;

    }
    else{
        [cell.lbl_sr_value setHidden:YES];
        [cell.lbl_sr setHidden:YES];
        cell.cons_1.constant = 0;
        cell.cons_11.constant = 0;    }
    
    //-------------- Add or Remove estimate_id ---------------
    if ([[arr_byFieldM objectAtIndex:1] isEqualToString:@"1"]) {
        [cell.lbl_estimateId_value setHidden:NO];
        [cell.lbl_estimate_id setHidden:NO];
        
        cell.cons_2.constant = HEIGHT;
        cell.cons_22.constant = HEIGHT;
        
    }
    else{
        [cell.lbl_estimateId_value setHidden:YES];
        [cell.lbl_estimate_id setHidden:YES];
        cell.cons_2.constant = 0;
        cell.cons_22.constant = 0;    }
    
    //-------------- Add or Remove Customer name ---------------
    if ([[arr_byFieldM objectAtIndex:2] isEqualToString:@"1"]) {
        [cell.lbl_customerName_value setHidden:NO];
        [cell.lbl_customer_name setHidden:NO];
        
        cell.cons_3.constant = HEIGHT;
        cell.cons_33.constant = HEIGHT;
        
    }
    else{
        [cell.lbl_customerName_value setHidden:YES];
        [cell.lbl_customer_name setHidden:YES];
        cell.cons_3.constant = 0;
        cell.cons_33.constant = 0;
        
    }
    
    //-------------- Add or Remove Sales Person ---------------
    if ([[arr_byFieldM objectAtIndex:3] isEqualToString:@"1"]) {
        [cell.lbl_salesPerson_value setHidden:NO];
        [cell.lbl_salaePerson setHidden:NO];
        
        cell.cons_4.constant = HEIGHT;
        cell.cons_44.constant = HEIGHT;
        
    }
    else{
        [cell.lbl_salesPerson_value setHidden:YES];
        [cell.lbl_salaePerson setHidden:YES];
        cell.cons_4.constant = 0;
        cell.cons_44.constant = 0;
        
    }
        //-------------- Add or Remove Customer Address ---------------
        if ([[arr_byFieldM objectAtIndex:4] isEqualToString:@"1"]) {
            [cell.lbl_customerAdd_value setHidden:NO];
            [cell.lbl_customerAddress setHidden:NO];
            
            cell.cons_5.constant = HEIGHT;
            cell.cons_55.constant = HEIGHT;
            
        }
        else{
            [cell.lbl_customerAdd_value setHidden:YES];
            [cell.lbl_customerAddress setHidden:YES];
            cell.cons_5.constant = 0;
            cell.cons_55.constant = 0;
            
        }
        //-------------- Add or Remove Total Amount ---------------
        if ([[arr_byFieldM objectAtIndex:5] isEqualToString:@"1"]) {
            [cell.lbl_totalAmt setHidden:NO];
            [cell.lbl_totalAmt_value setHidden:NO];
            
            cell.cons_6.constant = HEIGHT;
            cell.cons_66.constant = HEIGHT;
            
        }
        else{
            [cell.lbl_totalAmt setHidden:YES];
            [cell.lbl_totalAmt_value setHidden:YES];
            cell.cons_6.constant = 0;
            cell.cons_66.constant = 0;
            
        }
        
        //-------------- Add or Remove Status ---------------
        if ([[arr_byFieldM objectAtIndex:6] isEqualToString:@"1"]) {
            [cell.lbl_status setHidden:NO];
            [cell.lbl_status_value setHidden:NO];
            
            cell.cons_7.constant = HEIGHT;
            cell.cons_77.constant = HEIGHT;
            
        }
        else{
            [cell.lbl_status setHidden:YES];
            [cell.lbl_status_value setHidden:YES];
            cell.cons_7.constant = 0;
            cell.cons_77.constant = 0;
            
        }
        //-------------- Add or Remove Enable/Disable ---------------
        if ([[arr_byFieldM objectAtIndex:7] isEqualToString:@"1"]) {
            [cell.lbl_enable_value setHidden:NO];
            [cell.lbl_enable setHidden:NO];
            
            cell.cons_8.constant = HEIGHT;
            cell.cons_88.constant = HEIGHT;
            
        }
        else{
            [cell.lbl_enable_value setHidden:YES];
            [cell.lbl_enable setHidden:YES];
            cell.cons_8.constant = 0;
            cell.cons_88.constant = 0;
            
        }
        
        return cell;

    }
    else if(tableView == _tblView_export){
        
        ExportTableViewCell *export_cell = [_tblView_export dequeueReusableCellWithIdentifier:CELL_EXPORT];
        if (export_cell == nil) {
            export_cell = [[ExportTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CELL_EXPORT];
        }
        export_cell.lbl_exportType.text = arr_export[indexPath.row];
        return export_cell;
    }
    else{
        //viewByFeild
        
        //dequeueReusableCellWithIdentifier:forIndexPath
        ViewByFeildTableViewCell *my_cell = [_tblView_byFeild dequeueReusableCellWithIdentifier:CELL_VIEW_BY_FIELD];
        if (my_cell == nil) {
            my_cell = [[ViewByFeildTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CELL_VIEW_BY_FIELD];
        }

        my_cell.lbl_feildValue.text = arr_byFeildContent[indexPath.row];
        NSLog(@"%@",arr_byFeildContent[indexPath.row]);
        my_cell.btn_checked.tag = indexPath.row;
        [my_cell.btn_checked addTarget:self action:@selector(btn_CheckedAction:) forControlEvents:UIControlEventTouchUpInside];

        return my_cell;
    }
    return nil;
}


#pragma mark- IBACTION
#pragma mark-

- (IBAction)btn_Action_OnCancelSearch:(id)sender {
    
    _view_search.hidden = YES;
    _view_below_navigation.hidden = NO;
    
}
- (IBAction)btn_ActionOnSearch:(id)sender {
    
    _view_search.hidden = NO;
    _view_below_navigation.hidden = YES;
    
}

- (IBAction)action_viewByField:(id)sender {
    toggle_ViewByField = !toggle_ViewByField;
    if (toggle_ViewByField) {
        
        _view_alert.hidden = NO;
        [_tblView_byFeild reloadData];
    }
    else{
        _view_alert.hidden = YES;
        
    }
    
}



- (IBAction)action_exportClicked:(UIButton *)sender {
    
    toggle_export = !toggle_export;
    if (toggle_export) {
        
        _view_export.hidden = NO;
        [_tblView_export reloadData];
    }
    else{
        _view_export.hidden = YES;
        
    }
    
}

- (IBAction)action_expandClicked:(id)sender {
    toggle_expand = !toggle_expand;
    if (toggle_expand) {
        _cons_tblView_height.constant = self.view.frame.size.height - 120 - 40;
    }
    else{
        _cons_tblView_height.constant = self.view.frame.size.height - 120 ;

    }
    
}


#pragma mark- Delegate Method Search
#pragma mark-
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    if ([searchText isEqualToString:@""])
    {
        isSearch = NO;
    }
    else
    {
        isSearch= YES;
    }
    if (searchBar == self.searchBar)
    {
        
        NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", searchBar.text];

        arr_searchContent = [arrDemoM filteredArrayUsingPredicate:filterPredicate];
        NSLog(@"newSearch %@", arr_searchContent);
        [_tbl_estimate reloadData];
    }
    else
    {
        
        NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", searchBar.text];

        arr_searchContent = [arrDemoM filteredArrayUsingPredicate:filterPredicate];
        NSLog(@"newSearch %@", arr_searchContent);
        [_tbl_estimate reloadData];
    }
    
}




@end
