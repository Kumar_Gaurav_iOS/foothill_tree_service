//
//  LoginVC.h
//  Foothill
//
//  Created by coding Brains on 07/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginVC : UIViewController

@property (strong, nonatomic) IBOutlet UIView *view_forgotPassword;

@property (strong, nonatomic) IBOutlet UIButton *btn_login;
- (IBAction)btn_loginAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *view_bg;
- (IBAction)btn_forgotPwdAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtf_email;
@property (strong, nonatomic) IBOutlet UITextField *txtf_password;
@property (strong, nonatomic) IBOutlet UIButton *btn_forgotPassword;
@property (strong, nonatomic) IBOutlet UIButton *btn_showPwd;

@property (strong, nonatomic) IBOutlet UILabel *lbl_heading;

//---------------* Forgot Password *-------------------
@property (strong, nonatomic) IBOutlet UITextField *txtf_email_forgotPwd;
@property (strong, nonatomic) IBOutlet UIButton *btn_submit_forgotpwd;
- (IBAction)btn_submitAction_forgotPwd:(id)sender;
- (IBAction)btn_closeAction_forgotPwd:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *view_forgotPwdBG;


@end

