//
//  LoginVC.m
//  Foothill
//
//  Created by coding Brains on 07/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import "LoginVC.h"
#import "Constant.h"

@interface LoginVC ()<UITextFieldDelegate>
{
    NSDictionary *dictLogin;
    NSString *str_memberType;
    BOOL isShowPwd_toggle;
}

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self intializesAllVariables];
    // Do any additional setup after loading the view.
}


-(void)intializesAllVariables{
    // do initializes
    dictLogin = [[NSDictionary alloc]init];
    isShowPwd_toggle = NO;
    //Login Your Account
    //Forgot Your Password
    _lbl_heading.text = @"Login Your Account";
    
    self.view_forgotPassword.hidden = YES;
    self.view_bg.hidden = NO;
    self.btn_forgotPassword.hidden = NO;
    self.btn_submit_forgotpwd.hidden = YES;


    _view_bg.layer.cornerRadius = 10;
    self.view_forgotPwdBG.layer.cornerRadius = 10;
    
    self.btn_login.layer.cornerRadius = 18;
    self.btn_submit_forgotpwd.layer.cornerRadius = 18;

    
    //-------------- * Add  Gesture for keyboard hiding * ------------
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    [self.btn_showPwd addTarget:self action:@selector(show_Password) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:_view_bg.bounds];
    _view_bg.layer.masksToBounds = NO;
    _view_bg.layer.shadowColor = [UIColor blackColor].CGColor;
    _view_bg.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    _view_bg.layer.shadowOpacity = 0.5f;
    _view_bg.layer.shadowRadius = 15;

    _view_bg.layer.shadowPath = shadowPath.CGPath;
    
    
    UIBezierPath *shadowPath_btn = [UIBezierPath bezierPathWithRect:_btn_login.bounds];
    _btn_login.layer.masksToBounds = NO;
    _btn_login.layer.shadowColor = [UIColor blackColor].CGColor;
    _btn_login.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    _btn_login.layer.shadowOpacity = 0.5f;
    _btn_login.layer.shadowRadius = 15;
    _btn_login.layer.shadowPath = shadowPath_btn.CGPath;
    
    
    UIBezierPath *shadowPath_Forgot = [UIBezierPath bezierPathWithRect:_view_forgotPwdBG.bounds];
    _view_forgotPwdBG.layer.masksToBounds = NO;
    _view_forgotPwdBG.layer.shadowColor = [UIColor blackColor].CGColor;
    _view_forgotPwdBG.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    _view_forgotPwdBG.layer.shadowOpacity = 0.5f;
    _view_forgotPwdBG.layer.shadowRadius = 15;
    
    _view_bg.layer.shadowPath = shadowPath_Forgot.CGPath;
    
    
    UIBezierPath *shadowPath_Forgot_btn = [UIBezierPath bezierPathWithRect:_btn_submit_forgotpwd.bounds];
    _btn_submit_forgotpwd.layer.masksToBounds = NO;
    _btn_submit_forgotpwd.layer.shadowColor = [UIColor blackColor].CGColor;
    _btn_submit_forgotpwd.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    _btn_submit_forgotpwd.layer.shadowOpacity = 0.5f;
    _btn_submit_forgotpwd.layer.shadowRadius = 15;
    _btn_submit_forgotpwd.layer.shadowPath = shadowPath_Forgot_btn.CGPath;
}



-(void)dismissKeyboard
{
    [self.txtf_email resignFirstResponder];
    [self.txtf_password resignFirstResponder];
    [self.txtf_email_forgotPwd resignFirstResponder];

}

-(void)show_Password{
    
    isShowPwd_toggle = !isShowPwd_toggle;//Change the status
    if (isShowPwd_toggle) {
        UIImage * img = [UIImage imageNamed:@"show"];
        [_btn_showPwd setImage:img forState:UIControlStateNormal];
        self.txtf_password.secureTextEntry  = YES;

    }
    else{
        UIImage * img = [UIImage imageNamed:@"hide"];
        [_btn_showPwd setImage:img forState:UIControlStateNormal];
        self.txtf_password.secureTextEntry  = NO;
    }
}

#pragma mark - TextField Delegate
#pragma mark -

//- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//    if (textField == _emailTF) {
//        [textField resignFirstResponder];
//        _usernameUVIew.backgroundColor=[[UIColor lightGrayColor] colorWithAlphaComponent:0.2];
//        _passwordUView.backgroundColor=[UIColor darkGrayColor];
//        [_passwTF becomeFirstResponder];
//    }
//    else if (textField == _passwTF) {
//
//        [textField resignFirstResponder];
//
//    }
//    return YES;
//}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
 
    if ([textField.text  isEqual: @"Email"]) {
        self.txtf_email.text = @"";
    }
    
    else {

        
    }
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    
    [self.view endEditing:YES];
    return YES;
}



- (IBAction)btn_loginAction:(id)sender {

//    [self navigateToHomeFromLogin];
    if (![self.txtf_email.text isEqualToString:@""] && ![self.txtf_password.text isEqualToString:@""]) {
        [self loginAction];

    }
    else{
        [self loginAction];

    }

}



- (IBAction)btn_submitAction_forgotPwd:(id)sender {
    self.view_forgotPassword.hidden = YES;
    self.view_bg.hidden = NO;
    self.btn_forgotPassword.hidden = NO;
    self.btn_submit_forgotpwd.hidden = YES;
    _lbl_heading.text = @"Login Your Account";
    self.btn_login.hidden = NO;

}

- (IBAction)btn_closeAction_forgotPwd:(id)sender {
    
    self.view_forgotPassword.hidden = YES;
    self.view_bg.hidden = NO;
}
- (IBAction)btn_forgotPwdAction:(id)sender {
    
    _lbl_heading.text = @"Forgot Your Password";
    self.view_forgotPassword.hidden = NO;
    self.view_bg.hidden = YES;
    self.btn_forgotPassword.hidden = YES;
    self.btn_submit_forgotpwd.hidden = NO;
    self.btn_login.hidden = YES;

}


#pragma mark - API'S
#pragma mark -
-(void)loginAction{
    
    @try{
        
        if([[URLConfiguration sharedInstance] isInternetAvailable])
        {
            [KSToastView ks_showToast:@"Internet connectivity issue" delay:0.1f];
            return;
        }
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSMutableDictionary *params =[[NSMutableDictionary alloc]init];
        
//        [params setObject:self.txtf_email.text forKey:@"Email"];
        [params setObject:@"yovewec@alexbox.online" forKey:@"Email"];
        [params setObject:@"Ashish#1111" forKey:@"Password"];

        //yovewec@alexbox.online
        //Ashish#1111
        
//        [params setObject:self.txtf_password.text forKey:@"Password"];
        [params setObject:@"FoothillTokenMaster" forKey:@"DeviceToken"];
        [params setObject:APIKey forKey:@"apiKey"];
        [params setObject:@"ios" forKey:@"deviceType"];
        
        NSLog(@"%@",params);
        NSMutableString* parameterString = [NSMutableString string];
        for(NSString* key in [params allKeys])
        {
            if ([parameterString length]) {
                
                [parameterString appendString:@"&"];
            }
            [parameterString appendFormat:@"%@=%@",key, params[key]];
        }
        NSString* urlString = [NSString stringWithFormat:@"%@GetLogin",BaseUrl];
        NSURL* url = [NSURL URLWithString:urlString];
        
        NSURLSession* session =[NSURLSession sharedSession];
        NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[parameterString dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPShouldHandleCookies:NO];
        
        NSURLSessionDataTask* task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(error)
            {
                //do something
                NSLog(@"%@", error);
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSError *myError = nil;
                    
                    NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                    NSLog(@"%@",requestReply);
                    NSData *data = [requestReply dataUsingEncoding:NSUTF8StringEncoding];
                    NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                                 options:kNilOptions
                                                                                   error:&myError];
                    NSMutableDictionary*dic_response=[[NSMutableDictionary alloc]init];
                    
                    NSLog(@"%@",jsonResponse);
                    NSLog(@"%@",error);
                    
                    // NSURLResponse* response = // the response, from somewhere
                    
                    
                    //-----------* Get Header data from response *-----------
                    NSDictionary * headers = [(NSHTTPURLResponse *)response allHeaderFields];
                    //-------------------------------------------------------
                    //                       -------
                    //-------------* Get status from response *--------------
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                    long response_status = (long)[httpResponse statusCode];
                    NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
     
                    //------------ Response Code --------------
                    //200 --> succuess
                    //202,400 or other --> Failed in response
                    if(response_status == 200) {
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        
//                        if ([[jsonResponse objectForKey:@"status"] isEqualToString:@"200"]) {
                            self->dictLogin = [jsonResponse objectForKey:@"Data"];
                            
                            self->str_memberType = [self->dictLogin objectForKey:@"Name"];
                            //FullName
                            [[NSUserDefaults standardUserDefaults] setValue:[self->dictLogin objectForKey:@"FullName"]  forKey:@"FullName"];
                            
                            //ImagePath
                            [[NSUserDefaults standardUserDefaults] setValue:[self->dictLogin objectForKey:@"ImagePath"]  forKey:@"ImagePath"];
                            
                            //RollId
                            [[NSUserDefaults standardUserDefaults] setValue:[self->dictLogin objectForKey:@"RollId"]  forKey:@"RollId"];
                            
                            //Name
                            [[NSUserDefaults standardUserDefaults] setValue:self->str_memberType  forKey:@"Name"];
                            
                            //Id
                            [[NSUserDefaults standardUserDefaults] setValue:[self->dictLogin objectForKey:@"Id"]  forKey:@"Id"];
                            
                            //Email
                            [[NSUserDefaults standardUserDefaults] setValue:[self->dictLogin objectForKey:@"Email"]  forKey:@"Email"];
                            
                            //PhoneNumber
                            [[NSUserDefaults standardUserDefaults] setValue:[self->dictLogin objectForKey:@"PhoneNumber"]  forKey:@"PhoneNumber"];
                            
                            NSLog(@"PhoneNumber %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"PhoneNumber"]);
                            NSLog(@"Email %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"Email"]);
                            
                            NSLog(@"Id %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"Id"]);
                            
                            NSLog(@"Name %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"Name"]);
                            
                            NSLog(@"RollId %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"RollId"]);
                            NSLog(@"ImagePath %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"ImagePath"]);
                            NSLog(@"FullName %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"FullName"]);
                            
                            
                            //------------ Login with particlar member -----------
                            if ([self->str_memberType isEqualToString:Crew]) {
                                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:isLogin_Crew];
                            }
                            else if ([self->str_memberType isEqualToString:Estimator]) {
                                [[NSUserDefaults standardUserDefaults]setBool:YES forKey :isLogin_Estimator];
                            }
                            else{
                                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:isLogin_Foreman];
                                
                            }
                            
                            
                            [self navigateToHomeFromLogin];
                  
                            //usersCredit
                            
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            NSLog([[NSUserDefaults standardUserDefaults]boolForKey:@"isLogin"]?@"YES":@"NO");
                            
                            NSLog(@"%@",dic_response);
                            
                            //------------- Once Executed code ----------------
//                            static dispatch_once_t once;
//                            dispatch_once(&once, ^ {
//                                NSLog(@"Do it once");
//                                
//                                
//                            });
                            
        
                        
                    }
                    else
                    {
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        
                        if ([[jsonResponse objectForKey:@"flag"] isEqualToString:@"unsuccess"]) {
                            UIAlertController * alert=   [UIAlertController
                                                          alertControllerWithTitle:@"Alert"
                                                          message:@"Error to like!"
                                                          preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* yesButton = [UIAlertAction
                                                        actionWithTitle:@"ok"
                                                        style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                                        {
                                                            //Handel your yes please button action here
                                                        }];
                            
                            [alert addAction:yesButton];
                            [self presentViewController:alert animated:YES completion:nil];
                        }
                        
                        
                    }
                    
                });
            }
        }];
        [task resume];
    }//try
    
    @catch (NSException *exception) {
        NSLog(@"exception at btn_Recordings_like_clicked :%@",exception);
    }
    @finally{
        
    }
}

-(void)navigateToHomeFromLogin{
    NSString * storyboardName = @"";
    UIStoryboard *storyboard;
    if ([self->str_memberType isEqualToString:@"Crew"]) {
        storyboardName = @"Crew";
    }
    if ([self->str_memberType isEqualToString:@"Estimator"]) {
        storyboardName = @"Estimator";

    }
    if ([self->str_memberType isEqualToString:@"Foreman"]) {
        storyboardName = @"Foreman";
    }
    
    
    storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"DEMORootViewController"];
    [self presentViewController:vc animated:YES completion:nil];
    
}


@end
