//
//  RootVC.m
//  MyCoffee
//
//  Created by KUMAR GAURAV on 23/07/18.
//  Copyright © 2018 KUMAR GAURAV. All rights reserved.
//

#import "RootVC.h"
#import "LoginVC.h"
#import "DEMORootViewController.h"
#import "Constant.h"

@interface RootVC ()
{
    NSUserDefaults*defaults_userdata;

}
@end

@implementation RootVC
//
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString * storyboardName = @"";
    UIStoryboard *storyboard;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:isLogin_Crew]) {
        storyboardName = Crew;
        storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
        UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"DEMORootViewController"];
        [self presentViewController:vc animated:YES completion:nil];

    }
    if ([[NSUserDefaults standardUserDefaults] boolForKey:isLogin_Estimator]) {
        storyboardName = Estimator;
        storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
        UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"DEMORootViewController"];
        [self presentViewController:vc animated:YES completion:nil];

        
    }
    if ([[NSUserDefaults standardUserDefaults] boolForKey:isLogin_Foreman]) {
        storyboardName = Foreman;
        storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
        UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"DEMORootViewController"];
        [self presentViewController:vc animated:YES completion:nil];

    }
    else{
        DEMORootViewController *navVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        [self presentViewController:navVC animated:YES completion:nil];
    }
    
  

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
