//
//  DEMOLeftMenuViewController.m
//  RESideMenuStoryboards
//
//  Created by Roman Efimov on 10/9/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import "DEMOLeftMenuViewController.h"
#import "UIViewController+RESideMenu.h"
#import "LeftMenuTableViewCell.h"
#import "LoginVC.h"
#import "Constant.h"

@interface DEMOLeftMenuViewController ()
{
    NSArray *arrMenuItem_Crew,*arrMenuItem_Estimator,*arrMenuItem_Foreman;
    NSString *strUserName;
}
@property (strong, readwrite, nonatomic) UITableView *tableView;

@end

@implementation DEMOLeftMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self initializesAllVariables];
}


-(void)initializesAllVariables{
    
    arrMenuItem_Estimator = [[NSArray alloc]initWithObjects:@"Dashboard",@"Estimate Details", nil];
    arrMenuItem_Crew = [[NSArray alloc]initWithObjects:@"Dashboard",@"Previous Job Details", nil];
    arrMenuItem_Foreman = [[NSArray alloc]initWithObjects:@"Dashboard",@"Estimate Details",@"About Us", nil];
//    _imgUser.layer.cornerRadius=_imgUser.frame.size.width/2;
//
//    strUserName = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
//    _lblUserName.text = strUserName;
//
//    _lbl_userName.text = strUserName;
//    _lbl_email.text = [NSString stringWithFormat:@"%@@gmail.com",strUserName];
//    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"strCredit"] == [NSNull class]) {
//        _lbl_Credit.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"strCredit"];
//    }
//    else{
////        _lbl_Credit.text = @"0";
//    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isLogin"]) {
//        [_btnSignOut setTitle:@"Sign Out" forState:UIControlStateNormal];
    }
    else{

    }

}

#pragma mark -
#pragma mark UITableView Delegate
//Crew_Previous_JobVC
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog([[NSUserDefaults standardUserDefaults]boolForKey:@"arrDidSelectArrayM"]?@"Yes":@"No");

    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"Name"] isEqualToString:Crew]) {
        
        switch (indexPath.row) {
            case 0:
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"Crew_HomeVC"]]
                                                             animated:YES];
                [self.sideMenuViewController hideMenuViewController];
                break;
            case 1:
                    [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"Crew_Previous_JobVC"]]
                                                                 animated:YES];
                    [self.sideMenuViewController hideMenuViewController];
              
                break;
    
            default:
                break;
        }
        
    }
    else  if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"Name"] isEqualToString:Estimator]) {
        
        switch (indexPath.row) {
            case 0:
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"Estimator_HomeVC"]]
                                                             animated:YES];
                [self.sideMenuViewController hideMenuViewController];
                break;
            case 1:
                    [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"Estimator_HomeVC"]]
                                                                 animated:YES];
                    [self.sideMenuViewController hideMenuViewController];
              
                break;
                
            default:
                break;
        }
        
    }
    else  {
        
        switch (indexPath.row) {
            case 0:
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"Crew_HomeVC"]]
                                                             animated:YES];
                [self.sideMenuViewController hideMenuViewController];
                break;
            case 1:
                    [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"Crew_Previous_JobVC"]]
                                                                 animated:YES];
                    [self.sideMenuViewController hideMenuViewController];
               
                break;
                
            default:
                break;
        }
    }
    
  //  }
    
}

-(void)showAlert{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Alert"
                                  message:@"Please Choose store First!"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Ok"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    //Handel your yes please button action here
                                    [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"HomeMainVC"]]
                                                                                 animated:YES];
                                    [self.sideMenuViewController hideMenuViewController];
                                }];
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       
                                   }];
    [alert addAction:cancelButton];
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}


#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
 //    return 1;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    long intArrcount;
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"Name"] isEqualToString:@"Crew"]) {
        intArrcount = arrMenuItem_Crew.count;

    }
    else  if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"Name"] isEqualToString:@"Estimator"]) {
        intArrcount = arrMenuItem_Estimator.count;

    }
    else  {
        intArrcount = arrMenuItem_Foreman.count;

    }
    return intArrcount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"LeftMenuCell";//leftMenu_cell
    
    LeftMenuTableViewCell *cell = [_tblSideMenu dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[LeftMenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"Name"] isEqualToString:@"Crew"]) {
        cell.lbl_menuItem.text = arrMenuItem_Crew[indexPath.row];

    }
    else  if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"Name"] isEqualToString:@"Estimator"]) {
        cell.lbl_menuItem.text = arrMenuItem_Estimator[indexPath.row];
        
    }
    else  if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"Name"] isEqualToString:@"Foreman"]) {
        cell.lbl_menuItem.text = arrMenuItem_Foreman[indexPath.row];
        
    }
    
    cell.lbl_menuItem.textColor = UIColor.blackColor;
    
    return cell;
}





@end
