//
//  LeftMenuTableViewCell.h
//  MyCoffee
//
//  Created by KUMAR GAURAV on 23/07/18.
//  Copyright © 2018 KUMAR GAURAV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftMenuTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl_menuItem;

@end
