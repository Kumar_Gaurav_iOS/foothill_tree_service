//
//  DEMOLeftMenuViewController.m
//  RESideMenuStoryboards
//
//  Created by Roman Efimov on 10/9/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import "DEMOLeftMenuViewController.h"
#import "UIViewController+RESideMenu.h"
#import "LeftMenuTableViewCell.h"
#import "LoginVC.h"
@interface DEMOLeftMenuViewController ()
{
    NSArray *arrMenuItem;
    NSString *strUserName;
}
@property (strong, readwrite, nonatomic) UITableView *tableView;

@end

@implementation DEMOLeftMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self initializesAllVariables];
}


-(void)initializesAllVariables{
    
//    arrMenuItem = [[NSArray alloc]initWithObjects:@"Home",@"New Order",@"No Click Coffee",@"Store Menu",@"Gift Your Self",@"Gift Your Friend",@"Order History",@"Stay Healthy",@"Settings",@"About Us", nil];
    arrMenuItem = [[NSArray alloc]initWithObjects:@"Home",@"New Order",@"No Click Coffee",@"Store Menu",@"Gift Your Self",@"Gift Your Friend",@"Order History",@"Stay Healthy",@"Settings",@"About Us", nil];

    
    _lblUserName.text=@"Demo";
    _imgUser.layer.cornerRadius=_imgUser.frame.size.width/2;
    
    strUserName = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
    _lblUserName.text = strUserName;
    _lbl_Credit.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"strCredit"];

}

#pragma mark -
#pragma mark UITableView Delegate
//HomeMainVC
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //arrDidSelectArrayM
    NSLog([[NSUserDefaults standardUserDefaults]boolForKey:@"arrDidSelectArrayM"]?@"Yes":@"No");

    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"arrDidSelectArrayM"]) {
//        @"Home",@"New Order",@"No Click Coffee",@"Store Menu",@"Gift Your Self",@"Gift Your Friend",@"Order History",@"Stay Healthy",@"Settings",@"About Us"
        switch (indexPath.row) {
            case 0:
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"HomeMainVC"]]
                                                             animated:YES];
                [self.sideMenuViewController hideMenuViewController];
                break;
            case 1:
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"]]
                                                             animated:YES];
                [self.sideMenuViewController hideMenuViewController];
                break;
      
            case 2:
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"CoffeeOrderListVC"]]
                                                             animated:YES];
                [self.sideMenuViewController hideMenuViewController];
                break;
            case 3:
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"StoreViewController"]]
                                                             animated:YES];//IntroVC//StoreViewController
                [self.sideMenuViewController hideMenuViewController];
                break;
            case 8:
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"GeneralNotificationViewController"]]
                                                             animated:YES];
                [self.sideMenuViewController hideMenuViewController];
                break;
            case 5:
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"BlogTwoViewController"]]
                                                             animated:YES];
                [self.sideMenuViewController hideMenuViewController];
                break;
            default:
                break;
        }
    }
    
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return arrMenuItem.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"LeftMenuCell";
    
    LeftMenuTableViewCell *cell = [_tblSideMenu dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[LeftMenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }

    cell.lblMenuItem.text = arrMenuItem[indexPath.row];
    
    return cell;
}

- (IBAction)BtnSignOutAction:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert!" message:@"Do you want to logout ?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *yes = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                         {
                             [self logOutMethod];
                         }];
    UIAlertAction *no = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:no];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)logOutMethod
{
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        [defs removeObjectForKey:key];
    }
    [defs synchronize];
    
    LoginVC *navVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    [self presentViewController:navVC animated:YES completion:nil];
}
@end
