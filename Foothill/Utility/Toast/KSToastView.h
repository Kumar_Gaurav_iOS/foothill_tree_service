//
//  KSToastView.h


#import <UIKit/UIKit.h>

typedef void (^KSToastBlock)(void);

@interface KSToastView : UIView

/**
 *  ToastView Config
 */
+ (void)ks_setAppearanceBackgroundColor:(UIColor *)backgroundColor;
+ (void)ks_setAppearanceCornerRadius:(CGFloat)cornerRadius;
+ (void)ks_setAppearanceMaxHeight:(CGFloat)maxHeight;
+ (void)ks_setAppearanceMaxWidth:(CGFloat)maxWidth;
+ (void)ks_setAppearanceOffsetBottom:(CGFloat)offsetBottom;
+ (void)ks_setAppearanceTextAligment:(NSTextAlignment)textAlignment;
+ (void)ks_setAppearanceTextColor:(UIColor *)textColor;
+ (void)ks_setAppearanceTextFont:(UIFont *)textFont;
+ (void)ks_setAppearanceTextInsets:(UIEdgeInsets)textInsets;
+ (void)ks_setToastViewShowDuration:(NSTimeInterval)duration;

/**
 *  ToastView Show
 */
+ (void)ks_showToast:(id)toast;
+ (void)ks_showToast:(id)toast duration:(NSTimeInterval)duration;
+ (void)ks_showToast:(id)toast delay:(NSTimeInterval)delay;
+ (void)ks_showToast:(id)toast completion:(KSToastBlock)completion;
+ (void)ks_showToast:(id)toast duration:(NSTimeInterval)duration delay:(NSTimeInterval)delay;
+ (void)ks_showToast:(id)toast duration:(NSTimeInterval)duration completion:(KSToastBlock)completion;
+ (void)ks_showToast:(id)toast delay:(NSTimeInterval)delay completion:(KSToastBlock)completion;
+ (void)ks_showToast:(id)toast duration:(NSTimeInterval)duration delay:(NSTimeInterval)delay completion:(KSToastBlock)completion;

/**
 *  @Deprecated
 *  Please use + (void)ks_setAppearanceTextInsets:(UIEdgeInsets)textInsets;
 */
+ (void)ks_setAppearanceTextPadding:(CGFloat)textPadding __attribute__((deprecated));

@end
