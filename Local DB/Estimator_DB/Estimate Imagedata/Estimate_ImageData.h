//
//  Estimate_ImageData.h
//  Foothill
//
//  Created by coding Brains on 26/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Estimate_ImageData : NSManagedObject
//
@property (nullable, nonatomic, copy) NSString *image_path;
@property (nullable, nonatomic, copy) NSString *first_name;
@property (nullable, nonatomic, copy) NSString *estimate_id;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *second_name;
@end

NS_ASSUME_NONNULL_END
