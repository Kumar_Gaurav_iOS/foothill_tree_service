//
//  Estimate_ImageData.m
//  Foothill
//
//  Created by coding Brains on 26/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import "Estimate_ImageData.h"

@implementation Estimate_ImageData


@dynamic image_path;
@dynamic first_name;
@dynamic estimate_id;
@dynamic name;
@dynamic second_name;
@end
