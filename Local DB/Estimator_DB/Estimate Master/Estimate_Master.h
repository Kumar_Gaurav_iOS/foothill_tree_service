//
//  Estimate_Master.h
//  Foothill
//
//  Created by coding Brains on 28/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Estimate_Master : NSManagedObject

@property (nullable, nonatomic, copy) NSString *estimate_id_master;

@property (nullable, nonatomic, copy) NSString *approval_date_time_master;
@property (nullable, nonatomic, copy) NSString *attachment_id_master;
@property (nullable, nonatomic, copy) NSString *auto_approved_master;
@property (nullable, nonatomic, copy) NSString *complete_price_amt_master;
@property (nullable, nonatomic, copy) NSString *copy_to_job_date_timeStamp_master;
@property (nullable, nonatomic, copy) NSString *crew_notes_master;
@property (nullable, nonatomic, copy) NSString *customer_tags_id_master;

@property (nullable, nonatomic, copy) NSString *discount_amt_master;
@property (nullable, nonatomic, copy) NSString *discount_per_master;
@property (nullable, nonatomic, copy) NSString *entry_by_master;
@property (nullable, nonatomic, copy) NSString *entry_date_master;
@property (nullable, nonatomic, copy) NSString *estimate_display_master;
@property (nullable, nonatomic, copy) NSString *customer_id_master;
@property (nullable, nonatomic, copy) NSString *estimate_tag_id_master;
@property (nullable, nonatomic, copy) NSString *finist_date_time_stamp_master;
@property (nullable, nonatomic, copy) NSString *from_start_date_time_master;
@property (nullable, nonatomic, copy) NSString *hazards_notes_master;
@property (nullable, nonatomic, copy) NSString *is_active_master;
@property (nullable, nonatomic, copy) NSString *is_approval_master;
@property (nullable, nonatomic, copy) NSString *is_copy_to_job_master;
@property (nullable, nonatomic, copy) NSString *is_finish_master;
@property (nullable, nonatomic, copy) NSString *is_scheudle_master;
@property (nullable, nonatomic, copy) NSString *is_send_master;
@property (nullable, nonatomic, copy) NSString *is_start_master;
@property (nullable, nonatomic, copy) NSString *job_end_date_master;

@property (nullable, nonatomic, copy) NSString *job_paid_date_master;
@property (nullable, nonatomic, copy) NSString *job_start_date_master;
@property (nullable, nonatomic, copy) NSString *job_user_id_master;
@property (nullable, nonatomic, copy) NSString *modified_by_master;
@property (nullable, nonatomic, copy) NSString *omw_master;
@property (nullable, nonatomic, copy) NSString *modified_date_master;
@property (nullable, nonatomic, copy) NSString *omw_date_time_stamp_master;
@property (nullable, nonatomic, copy) NSString *previous_note_master;
@property (nullable, nonatomic, copy) NSString *private_notes_master;

@property (nullable, nonatomic, copy) NSString *sales_user_master;
@property (nullable, nonatomic, copy) NSString *scdehuled_date_timeStamp_master;
@property (nullable, nonatomic, copy) NSString *schedule_to_user_id_master;
@property (nullable, nonatomic, copy) NSString *send_date_time_stamp_master;
@property (nullable, nonatomic, copy) NSString *start_date_time_stamp_master;
@property (nullable, nonatomic, copy) NSString *status_master;
@property (nullable, nonatomic, copy) NSString *tax_amt_master;
@property (nullable, nonatomic, copy) NSString *tax_rate_master;
@property (nullable, nonatomic, copy) NSString *to_finishdate_time_master;
@property (nullable, nonatomic, copy) NSString *total_deduction_master;
@property (nullable, nonatomic, copy) NSString *total_price_master;


@end

NS_ASSUME_NONNULL_END
