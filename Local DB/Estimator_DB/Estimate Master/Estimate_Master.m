//
//  Estimate_Master.m
//  Foothill
//
//  Created by coding Brains on 28/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import "Estimate_Master.h"
@dynamic estimate_id_master;

@dynamic approval_date_time_master;
@dynamic attachment_id_master;
@dynamic auto_approved_master;
@dynamic complete_price_amt_master;
@dynamic copy_to_job_date_timeStamp_master;
@dynamic crew_notes_master;
@dynamic customer_tags_id_master;

@dynamic discount_amt_master;
@dynamic discount_per_master;
@dynamic entry_by_master;
@dynamic entry_date_master;
@dynamic *estimate_display_master;
@dynamic *customer_id_master;
@dynamic *estimate_tag_id_master;
@dynamic *finist_date_time_stamp_master;
@dynamic *from_start_date_time_master;
@dynamic *hazards_notes_master;
@dynamic *is_active_master;
@dynamic *is_approval_master;
@dynamic *is_copy_to_job_master;
@dynamic *is_finish_master;
@dynamic *is_scheudle_master;
@dynamic *is_send_master;
@dynamic *is_start_master;
@dynamic *job_end_date_master;

@dynamic *job_paid_date_master;
@dynamic *job_start_date_master;
@dynamic *job_user_id_master;
@dynamic *modified_by_master;
@dynamic *omw_master;
@dynamic *modified_date_master;
@dynamic *omw_date_time_stamp_master;
@dynamic *previous_note_master;
@dynamic *private_notes_master;

@dynamic *sales_user_master;
@dynamic *scdehuled_date_timeStamp_master;
@dynamic *schedule_to_user_id_master;
@dynamic *send_date_time_stamp_master;
@dynamic *start_date_time_stamp_master;
@dynamic *status_master;
@dynamic *tax_amt_master;
@dynamic *tax_rate_master;
@dynamic *to_finishdate_time_master;
@dynamic *total_deduction_master;
@dynamic *total_price_master;

@end
