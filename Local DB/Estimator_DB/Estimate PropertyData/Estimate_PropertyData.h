//
//  Estimate_PropertyData.h
//  Foothill
//
//  Created by coding Brains on 26/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Estimate_PropertyData : NSManagedObject
//@property (nullable, nonatomic, copy) NSString *estimate_id;

@property (nullable, nonatomic, copy) NSString *address_note;
@property (nullable, nonatomic, copy) NSString *attribute;
@property (nullable, nonatomic, copy) NSString *city;
@property (nullable, nonatomic, copy) NSString *company_name;
@property (nullable, nonatomic, copy) NSString *email;
@property (nullable, nonatomic, copy) NSString *first_name;
@property (nullable, nonatomic, copy) NSString *home_phone;

@property (nullable, nonatomic, copy) NSString *job_title;
@property (nullable, nonatomic, copy) NSString *last_name;
@property (nullable, nonatomic, copy) NSString *latitude;
@property (nullable, nonatomic, copy) NSString *mobile_number;
@property (nullable, nonatomic, copy) NSString *office_phone;
@property (nullable, nonatomic, copy) NSString *state;
@property (nullable, nonatomic, copy) NSString *street;
@property (nullable, nonatomic, copy) NSString *unit;
@property (nullable, nonatomic, copy) NSString *zip_code;

@end

NS_ASSUME_NONNULL_END
