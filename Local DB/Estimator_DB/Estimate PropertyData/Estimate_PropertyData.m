//
//  Estimate_PropertyData.m
//  Foothill
//
//  Created by coding Brains on 26/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import "Estimate_PropertyData.h"

@implementation Estimate_PropertyData



//@dynamic estimate_id;

@dynamic address_note;
@dynamic attribute;
@dynamic city;
@dynamic company_name;
@dynamic email;
@dynamic first_name;
@dynamic home_phone;

@dynamic job_title;
@dynamic last_name;
@dynamic latitude;
@dynamic mobile_number;
@dynamic office_phone;
@dynamic state;
@dynamic street;
@dynamic unit;
@dynamic zip_code;
@end
