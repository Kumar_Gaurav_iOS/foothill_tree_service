//
//  Estimate_TreeDetails.h
//  Foothill
//
//  Created by coding Brains on 26/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Estimate_TreeDetails : NSManagedObject

//@property (nullable, nonatomic, copy) NSString *estimate_id;

@property (nullable, nonatomic, copy) NSString *botanical_name;
@property (nullable, nonatomic, copy) NSString *building;
@property (nullable, nonatomic, copy) NSString *common_name;
@property (nullable, nonatomic, copy) NSString *dbh;
@property (nullable, nonatomic, copy) NSString *hardscape_id;
@property (nullable, nonatomic, copy) NSString *image_name;
@property (nullable, nonatomic, copy) NSString *image_path;

@property (nullable, nonatomic, copy) NSString *lat;
@property (nullable, nonatomic, copy) NSString *lng;
@property (nullable, nonatomic, copy) NSString *powerline;
@property (nullable, nonatomic, copy) NSString *rating;
@property (nullable, nonatomic, copy) NSString *tree_id;
@end

NS_ASSUME_NONNULL_END
