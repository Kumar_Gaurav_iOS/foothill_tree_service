//
//  Estimate_TreeDetails.m
//  Foothill
//
//  Created by coding Brains on 26/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import "Estimate_TreeDetails.h"

@implementation Estimate_TreeDetails

//@dynamic estimate_id;

@dynamic botanical_name;
@dynamic building;
@dynamic common_name;
@dynamic dbh;
@dynamic hardscape_id;
@dynamic image_name;
@dynamic image_path;

@dynamic lat;
@dynamic lng;
@dynamic powerline;
@dynamic rating;
@dynamic tree_id;

@end
