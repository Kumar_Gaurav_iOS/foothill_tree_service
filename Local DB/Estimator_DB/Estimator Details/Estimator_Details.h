//
//  Estimator_Details.h
//  Foothill
//
//  Created by coding Brains on 26/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Estimator_Details : NSManagedObject
@property (nullable, nonatomic, copy) NSString *address;
@property (nullable, nonatomic, copy) NSString *customer_name;
@property (nullable, nonatomic, copy) NSString *est_details_id;
@property (nullable, nonatomic, copy) NSString *estimate_id;
@property (nullable, nonatomic, copy) NSString *is_active;
@property (nullable, nonatomic, copy) NSString *property_id;
@property (nullable, nonatomic, copy) NSString *sales_user;
@property (nullable, nonatomic, copy) NSString *total_price;
@property (nullable, nonatomic, copy) NSString *status;


@end

NS_ASSUME_NONNULL_END
