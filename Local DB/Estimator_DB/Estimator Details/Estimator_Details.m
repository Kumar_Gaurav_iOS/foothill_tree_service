//
//  Estimator_Details.m
//  Foothill
//
//  Created by coding Brains on 26/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import "Estimator_Details.h"

@implementation Estimator_Details
@dynamic address;
@dynamic customer_name;
@dynamic est_details_id;
@dynamic estimate_id;
@dynamic is_active;
@dynamic property_id;
@dynamic sales_user;
@dynamic total_price;
@dynamic status;

@end
