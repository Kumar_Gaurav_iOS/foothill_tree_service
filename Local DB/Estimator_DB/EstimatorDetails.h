//
//  EstimatorDetails.h
//  Foothill
//
//  Created by coding Brains on 26/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface EstimatorDetails : NSObject
{
    //--------- Estimator ----------
        NSMutableArray *arrEstimator_MasterM;
        NSMutableArray *arrEstimat_ImageDataM;
        NSMutableArray *arrEstimat_PropertyDataM;
        NSMutableArray *arrEstimat_TreeDetailsM;



        NSDictionary *dicEstimator_Master;
}

//----------------* For Estimator *---------------
-(void)insert_AllData_DB_FromServer_Details:(NSArray*)dicEstimatorDetails;
-(NSMutableArray*)FetchDataFromDataBase;
- (void)deleteAllEntities:(NSString *)nameEntity;

+ (instancetype)sharedInstance;
- (instancetype)init;

@end

NS_ASSUME_NONNULL_END
