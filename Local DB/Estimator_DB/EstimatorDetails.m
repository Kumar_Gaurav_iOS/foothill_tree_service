//
//  EstimatorDetails.m
//  Foothill
//
//  Created by coding Brains on 26/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import "EstimatorDetails.h"
#import "Constant.h"


@implementation EstimatorDetails



+ (instancetype)sharedInstance
{
    static id instance_ = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance_ = [[self alloc] init];
    });
    
    return instance_;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}


#pragma mark - Estimator Details


-(void)insert_AllData_DB_FromServer_Details:(NSArray*)arr_EstimatesData{

    arrEstimator_MasterM = [[NSMutableArray alloc]init];
    arrEstimat_ImageDataM = [[NSMutableArray alloc]init];
    arrEstimat_TreeDetailsM = [[NSMutableArray alloc]init];
    arrEstimat_PropertyDataM = [[NSMutableArray alloc]init];

    for (int i = 0; i < arr_EstimatesData.count; i++) {
//
    Estimator_Details *myobj = [NSEntityDescription insertNewObjectForEntityForName:@"Estimator_Details" inManagedObjectContext:[[URLConfiguration sharedInstance] managedObjectContext]];

        myobj.estimate_id = [NSString stringWithFormat:@"%@",[[arr_EstimatesData objectAtIndex:i]valueForKey:@"EstimateId"]];

        myobj.property_id = [NSString stringWithFormat:@"%@",[[arr_EstimatesData objectAtIndex:i]valueForKey:@"PropertyId"]];

        myobj.status = [NSString stringWithFormat:@"%@",[[arr_EstimatesData objectAtIndex:i]valueForKey:@"Status"]];

        myobj.total_price = [NSString stringWithFormat:@"%@",[[arr_EstimatesData objectAtIndex:i]valueForKey:@"TotalPrice"]];

        myobj.address = [NSString stringWithFormat:@"%@",[[arr_EstimatesData objectAtIndex:i]valueForKey:@"Address"]];

        myobj.customer_name = [NSString stringWithFormat:@"%@",[[arr_EstimatesData objectAtIndex:i]valueForKey:@"CustomerName"]];

        myobj.sales_user = [NSString stringWithFormat:@"%@",[[arr_EstimatesData objectAtIndex:i]valueForKey:@"SalesUser"]];

        myobj.is_active = [NSString stringWithFormat:@"%@",[[arr_EstimatesData objectAtIndex:i]valueForKey:@"IsActive"]];
        
        NSError *error = nil;
        if (![myobj.managedObjectContext save:&error])
        {
            NSLog(@"Error");

        }
        else
        {
            NSLog(@"Stringing saving in database");
        }
    
        //1
        arrEstimator_MasterM = [[arr_EstimatesData objectAtIndex:i]valueForKey:@"EstimateMasterRow"];
        if (arrEstimator_MasterM.count > 0) {
            for (int i = 0; i < arrEstimator_MasterM.count; i++) {

                Estimate_Master *obj_master = [NSEntityDescription insertNewObjectForEntityForName:@"Estimate_Master" inManagedObjectContext:[[URLConfiguration sharedInstance] managedObjectContext]];

                obj_master.customer_id_master = [NSString stringWithFormat:@"%@",[[arrEstimator_MasterM objectAtIndex:i]valueForKey:@"CustomerId"]];
                
                obj_master.estimate_id_master = [NSString stringWithFormat:@"%@",[[arrEstimator_MasterM objectAtIndex:i]valueForKey:@"EstimateId"]];

                obj_master.customer_tags_id_master = [NSString stringWithFormat:@"%@",[[arrEstimator_MasterM objectAtIndex:i]valueForKey:@"CustomerTagsId"]];

                obj_master.estimate_display_master = [NSString stringWithFormat:@"%@",[[arrEstimator_MasterM objectAtIndex:i]valueForKey:@"EstimateDisplayName"]];

                obj_master.estimate_tag_id_master = [NSString stringWithFormat:@"%@",[[arrEstimator_MasterM objectAtIndex:i]valueForKey:@"EstimateTagId"]];

                obj_master.estimate_tag_id_master = [NSString stringWithFormat:@"%@",[[arrEstimator_MasterM objectAtIndex:i]valueForKey:@"PrivateNote"]];


                NSError *error = nil;
                if (![obj_master.managedObjectContext save:&error])
                {
                    NSLog(@"Error");

                }
                else
                {
                    NSLog(@"Stringing saving in database");
                }

            }
//
//        }
        
        //2
//        arrEstimat_PropertyDataM = [[arr_EstimatesData objectAtIndex:i]valueForKey:@"PropertyData"];
//        if (arrEstimat_PropertyDataM.count > 0) {
//            for (int i = 0; i < arrEstimat_PropertyDataM.count; i++) {
//
//                Estimate_PropertyData *obj_property = [NSEntityDescription insertNewObjectForEntityForName:@"Estimate_PropertyData" inManagedObjectContext:[[URLConfiguration sharedInstance] managedObjectContext]];
//
//                obj_property.city = [[URLConfiguration sharedInstance]checkNullForString:[[arr_EstimatesData objectAtIndex:i]valueForKey:@"City"]];
//
//                NSError *error = nil;
//                if (![obj_property.managedObjectContext save:&error])
//                {
//                    NSLog(@"Error");
//
//                }
//                else
//                {
//                    NSLog(@"Stringing saving in database");
//                }
//
//            }
//
//        }
        
        //3
//        arrEstimat_TreeDetailsM = [[arr_EstimatesData objectAtIndex:i]valueForKey:@"treedetails"];
//        if (arrEstimat_TreeDetailsM.count > 0) {
//
//            [self deleteAllEntities:@"Estimate_TreeDetails"];
//
//
//            for (int i = 0; i < arrEstimat_TreeDetailsM.count; i++) {
//
//                Estimate_TreeDetails *obj_treeDetails = [NSEntityDescription insertNewObjectForEntityForName:@"Estimate_TreeDetails" inManagedObjectContext:[[URLConfiguration sharedInstance] managedObjectContext]];
//
//
//                obj_treeDetails.tree_id =[NSString stringWithFormat:@"%@",[[arrEstimat_TreeDetailsM objectAtIndex:i]valueForKey:@"treeID"]];
//
//                NSLog(@"%@", obj_treeDetails.tree_id);
//
//                NSError *error = nil;
//                if (![obj_treeDetails.managedObjectContext save:&error])
//                {
//                    NSLog(@"Error");
//
//                }
//                else
//                {
//                    NSLog(@"Stringing saving in database");
//                }
//
//            }
//
//        }
    
        //4
//        arrEstimat_ImageDataM = [[arr_EstimatesData objectAtIndex:i]valueForKey:@"EstimateImagedata"];
//        if (arrEstimat_ImageDataM.count > 0) {
//            for (int i = 0; i < arrEstimat_ImageDataM.count; i++) {
//
//                Estimate_ImageData *obj_imageData = [NSEntityDescription insertNewObjectForEntityForName:@"Estimate_ImageData" inManagedObjectContext:[[URLConfiguration sharedInstance] managedObjectContext]];
//
//                obj_imageData.image_path = [[URLConfiguration sharedInstance]checkNullForString:[[arr_EstimatesData objectAtIndex:i]valueForKey:@"ImagePath"]];
//
//                NSError *error = nil;
//                if (![obj_imageData.managedObjectContext save:&error])
//                {
//                    NSLog(@"Error");
//
//                }
//                else
//                {
//                    NSLog(@"Stringing saving in database");
//                }
//
//            }
//
//        }
        
        
        }
    }
 
}

//
- (void)deleteAllEntities:(NSString *)nameEntity
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:nameEntity];

    NSManagedObjectContext *context = [[URLConfiguration sharedInstance] managedObjectContext];


    [fetchRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID

    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject *object in fetchedObjects)
    {
        [context deleteObject:object];
    }

    error = nil;
    [context save:&error];
}



-(NSMutableArray*)FetchDataFromDataBase{
    
    NSMutableArray*arr = [[NSMutableArray alloc]init];
    NSManagedObjectContext *context = [[URLConfiguration sharedInstance] managedObjectContext];
    
    context=[[URLConfiguration sharedInstance] managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Estimator_Details" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    
    NSArray  *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        NSLog(@"no object");
    }
    else
    {
        for(NSManagedObject* currentObj in fetchedObjects) {
            [arr addObject:currentObj];
            
//            NSString *str = [currentObj valueForKey:@"tree_id"];
        }}
    
    
    return arr;
    
}

@end
