//
//  Records.h
//  Foothill
//
//  Created by coding Brains on 26/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Records : NSManagedObject
@property (nullable, nonatomic, copy) NSString *name;

@end

NS_ASSUME_NONNULL_END
