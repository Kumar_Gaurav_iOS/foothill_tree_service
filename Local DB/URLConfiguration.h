//
//  URLConfiguration.h
//  Foothill
//
//  Created by coding Brains on 20/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN



@interface URLConfiguration : NSObject
{
    //--------- Estimator ----------
//    NSMutableArray *arrEstimator_DetailsM;
//    NSDictionary *dicEstimator_Master;

    int num;
    NSManagedObjectModel *managedObjectModel;
    NSManagedObjectContext *managedObjectContext;
    NSPersistentStoreCoordinator *persistentStoreCoordinator;

    NSString *str_estDetailsid;
    NSString *str_estimateMasterId;
    NSString *str_estItemId;
    
    NSString *str_estTreatMentType;
    NSString *str_estTreatMentId;
    NSString *str_estIsTaxable;
    NSString *str_estIsDiscount;
    NSString *str_estIsDiscountType;
    NSString *str_estTreeId;
    NSString *str_estEntryDate;
    NSString *str_estModifiedDate;
    NSString *str_estItemDescription;
    NSString *str_estIsApprove;
    NSString *str_estQty;
    NSString *str_estUnitPrice;
    NSString *str_estAmt;


}

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSManagedObjectModel  *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator  *persistentStoreCoordinator;
- (void)saveContext;

@property (readonly, strong) NSPersistentContainer *persistentContainer;


+ (instancetype)sharedInstance;
- (instancetype)init;
-(BOOL)isInternetAvailable;
-(void)ReadyToUse;
-(NSString*)checkNullNumber:(NSString*)value;
-(NSString*)checkNullForString:(NSString*)value;

//----------------* For Estimator *---------------
//-(void)insert_AllData_DB_FromServer_Details:(NSArray*)dicEstimatorDetails;
-(void)insert_AllData_DB_FromServer_Master_Details:(NSArray*)dicEstimatorMaster;
-(NSMutableArray*)FetchDataFromDataBase;


//----------------* For Estimator Master Details *---------------



@property( atomic , retain ) NSString *sync_value;
@property( atomic , retain ) NSString *not_sync_value;
@property( assign) BOOL isLoginFirst;

@end

NS_ASSUME_NONNULL_END
