//
//  URLConfiguration.m
//  Foothill
//
//  Created by coding Brains on 20/02/19.
//  Copyright © 2019 coding Brains. All rights reserved.
//

#import "URLConfiguration.h"
#import "Reachability.h"

@implementation URLConfiguration


@synthesize managedObjectContext;
@synthesize managedObjectModel;
@synthesize persistentStoreCoordinator;

@synthesize sync_value;
@synthesize not_sync_value;
+ (instancetype)sharedInstance
{
    static id instance_ = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance_ = [[self alloc] init];
    });
    
    return instance_;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

-(NSString*)checkNullForString:(NSString*)value
{
    if(value == (id)[NSNull null] || [value isEqualToString:@"NULL"] )
    {
        value = @"";
    }
    return value;
}



-(NSString*)checkNullNumber:(NSString*)value
{
    if(value == (id)[NSNull null] )
    {
        value = @"0";
    }
    return value;
}

-(BOOL)isInternetAvailable
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}


-(void)ReadyToUse
{
    num = 0;


}



#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"Demo_CoreData"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

- (NSManagedObjectContext *) managedObjectContext {
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    
    return managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    if (managedObjectModel != nil) {
        return managedObjectModel;
    }
    managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil] ;
    
    return managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
    
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"CoreDataExample.sqlite"]];
    
    NSError *error = nil;
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&error]) {
        // Handle error
    }
    
    return persistentStoreCoordinator;}

- (NSString *)applicationDocumentsDirectory {
    
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

#pragma mark - Estimator Details
//-(void)insert_AllData_DB_FromServer_Details:(NSArray*)arr_EstimatesData{
//    
//    arrEstimator_DetailsM = [[NSMutableArray alloc]init];
//    NSManagedObjectContext *context = [self managedObjectContext];
//    for (int num = 0; num< arr_EstimatesData.count; num++) {
//       arrEstimator_DetailsM = [[arr_EstimatesData objectAtIndex:num]valueForKey:@"EstimateDetails"];
//        
//        for (int innerLoop = 0; innerLoop < arrEstimator_DetailsM.count; innerLoop++) {
//            NSDictionary *dicEstimatorDetails = [[NSDictionary alloc]init];
//            dicEstimatorDetails = [arrEstimator_DetailsM objectAtIndex:innerLoop];
//            
//            if (arrEstimator_DetailsM.count>0) {
//                
//                // Create a new managed object
//                NSManagedObject *newDevice = [NSEntityDescription insertNewObjectForEntityForName:@"Estimator_Details" inManagedObjectContext:context];
////                if ([[dicEstimatorDetails valueForKey:@"EstDetailsId"] isEqualToString:[NSNull null]]) {
////                      [newDevice setValue:[dicEstimatorDetails valueForKey:@"EstDetailsId"] forKey:@"EstDetailsId"];//1
////                }
////                else{
//                
//                
//                str_estDetailsid = [NSString stringWithFormat:@"%@",[dicEstimatorDetails valueForKey:@"EstDetailsId"]];
//                
//                    [newDevice setValue:str_estDetailsid forKey:@"estDetailsId"];//1
//                //--------------*-----------*-----------*----------------
////                }
////                str_estimateMasterId = [dicEstimatorDetails valueForKey:@"EstimateMasterId"];
//                str_estimateMasterId = [NSString stringWithFormat:@"%@",[dicEstimatorDetails valueForKey:@"EstimateMasterId"]];
//
//                [newDevice setValue:str_estimateMasterId forKey:@"estimateMasterId"];//2
//                
//                //--------------*-----------*-----------*----------------
//
//                str_estItemId = [NSString stringWithFormat:@"%@",[dicEstimatorDetails valueForKey:@"ItemId"]];
//
//                [newDevice setValue:str_estItemId forKey:@"itemId"];//3
//                
//                //--------------*-----------*-----------*----------------
//                str_estQty = [NSString stringWithFormat:@"%@",[dicEstimatorDetails valueForKey:@"Qty"]];
//
//                [newDevice setValue:str_estQty forKey:@"qty"];//4
//                //--------------*-----------*-----------*----------------
//                str_estUnitPrice = [NSString stringWithFormat:@"%@",[dicEstimatorDetails valueForKey:@"UnitPrice"]];
//
//                [newDevice setValue:str_estUnitPrice forKey:@"unitPrice"];//5
//                //--------------*-----------*-----------*----------------
//                str_estAmt = [NSString stringWithFormat:@"%@",[dicEstimatorDetails valueForKey:@"Amt"]];
//
//                [newDevice setValue:str_estAmt forKey:@"amt"];//6
//                //--------------*-----------*-----------*----------------
//                str_estTreatMentType = [NSString stringWithFormat:@"%@",[dicEstimatorDetails valueForKey:@"TreatMentType"]];
//
//                [newDevice setValue:str_estTreatMentType forKey:@"treatMentType"];//7
//                //--------------*-----------*-----------*----------------
//                str_estTreatMentId = [NSString stringWithFormat:@"%@",[dicEstimatorDetails valueForKey:@"TreatMentId"]];
//
//                [newDevice setValue:str_estTreatMentId forKey:@"treatMentId"];//8
//                //--------------*-----------*-----------*----------------
//                str_estIsTaxable = [NSString stringWithFormat:@"%@",[dicEstimatorDetails valueForKey:@"IsTaxable"]];
//
//                [newDevice setValue:str_estIsTaxable forKey:@"isTaxable"];//9
//                //--------------*-----------*-----------*----------------
//                str_estIsDiscount = [NSString stringWithFormat:@"%@",[dicEstimatorDetails valueForKey:@"isDiscount"]];
//
//                [newDevice setValue:str_estIsDiscount forKey:@"isDiscount"];//10
//                //--------------*-----------*-----------*----------------
//                str_estIsDiscountType = [NSString stringWithFormat:@"%@",[dicEstimatorDetails valueForKey:@"IsDiscountType"]];
//
//                if ([str_estIsDiscountType isEqualToString:@"null"]) {
//                    [newDevice setValue:@"" forKey:@"isDiscountType"];//11
//
//                }
//                else{
//                    [newDevice setValue:str_estIsDiscountType forKey:@"isDiscountType"];//11
//
//                }
//                //--------------*-----------*-----------*----------------
//                str_estTreeId = [NSString stringWithFormat:@"%@",[dicEstimatorDetails valueForKey:@"TreeId"]];
//
//                [newDevice setValue:str_estTreeId forKey:@"treeId"];//12
//                //--------------*-----------*-----------*----------------
//                str_estEntryDate = [NSString stringWithFormat:@"%@",[dicEstimatorDetails valueForKey:@"EntryDate"]];
//
//                [newDevice setValue:str_estEntryDate forKey:@"entryDate"];//13
//                //--------------*-----------*-----------*----------------
//                str_estModifiedDate = [NSString stringWithFormat:@"%@",[dicEstimatorDetails valueForKey:@"ModifiedDate"]];
//
//                [newDevice setValue:str_estModifiedDate forKey:@"modifiedDate"];//14
//                //--------------*-----------*-----------*----------------
//                str_estItemDescription = [NSString stringWithFormat:@"%@",[dicEstimatorDetails valueForKey:@"ItemDescription"]];
//
//                [newDevice setValue:str_estItemDescription forKey:@"itemDescription"];//15
//                //--------------*-----------*-----------*----------------
//                str_estIsApprove = [NSString stringWithFormat:@"%@",[dicEstimatorDetails valueForKey:@"isApprove"]];
//
//                [newDevice setValue:str_estIsApprove forKey:@"isApprove"];//16
//                //--------------*-----------*-----------*----------------
//
//                NSError *error = nil;
//                // Save the object to persistent store
//                if (![context save:&error]) {
//                    NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
//                }
//                else{
//                    NSLog(@"Saved Data");
//                    
//                }
//            }
//        }
//        
//    }
//    
// 
//}

//-(NSMutableArray*)FetchDataFromDataBase{
//    
//    NSMutableArray*arr = [[NSMutableArray alloc]init];
//    NSManagedObjectContext *context = [self managedObjectContext];
//    
//    context=[self managedObjectContext];
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Records" inManagedObjectContext:context];
//    
//    [fetchRequest setEntity:entity];
//    NSError *error = nil;
//    
//    NSArray  *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
//    if (fetchedObjects == nil) {
//        NSLog(@"no object");
//    }
//    else
//    {
//        for(NSManagedObject* currentObj in fetchedObjects) {
//            [arr addObject:currentObj];
//            
//            //            NSLog(arrCoreData);
//        }}
//    
//  
//    return arr;
//    
//}


#pragma mark - Estimator Master Details
//-(void)insert_AllData_DB_FromServer_Master_Details:(NSArray*)arr_EstimatesData{
//
//    dicEstimator_Master = [[NSDictionary alloc]init];
//    NSManagedObjectContext *context = [self managedObjectContext];
//    for (int num = 0; num< arr_EstimatesData.count; num++) {
//        dicEstimator_Master = [[arr_EstimatesData objectAtIndex:num]valueForKey:@"EstimateMaster"];
//
//
//            if (dicEstimator_Master.count>0) {
//
//                // Create a new managed object
//                NSManagedObject *newDevice = [NSEntityDescription insertNewObjectForEntityForName:@"Records" inManagedObjectContext:context];
//                [newDevice setValue:[dicEstimator_Master valueForKey:@"EstimateId"] forKey:@"EstimateId"];//1
//                [newDevice setValue:[dicEstimator_Master valueForKey:@"EstimateMasterId"] forKey:@"EstimateMasterId"];//2
//                [newDevice setValue:[dicEstimator_Master valueForKey:@"ItemId"] forKey:@"ItemId"];//3
//
//                [newDevice setValue:[dicEstimator_Master valueForKey:@"Qty"] forKey:@"Qty"];//4
//                [newDevice setValue:[dicEstimator_Master valueForKey:@"UnitPrice"] forKey:@"UnitPrice"];//5
//                [newDevice setValue:[dicEstimator_Master valueForKey:@"Amt"] forKey:@"Amt"];//6
//
//                [newDevice setValue:[dicEstimator_Master valueForKey:@"TreatMentType"] forKey:@"TreatMentType"];//7
//                [newDevice setValue:[dicEstimator_Master valueForKey:@"TreatMentId"] forKey:@"TreatMentId"];//8
//                [newDevice setValue:[dicEstimator_Master valueForKey:@"IsTaxable"] forKey:@"IsTaxable"];//9
//
//                [newDevice setValue:[dicEstimator_Master valueForKey:@"IsDiscount"] forKey:@"IsDiscount"];//10
//                [newDevice setValue:[dicEstimator_Master valueForKey:@"IsDiscountType"] forKey:@"IsDiscountType"];//11
//                [newDevice setValue:[dicEstimator_Master valueForKey:@"TreeId"] forKey:@"TreeId"];//12
//                [newDevice setValue:[dicEstimator_Master valueForKey:@"EntryDate"] forKey:@"EntryDate"];//13
//
//                [newDevice setValue:[dicEstimator_Master valueForKey:@"ModifiedDate"] forKey:@"ModifiedDate"];//14
//                [newDevice setValue:[dicEstimator_Master valueForKey:@"ItemDescription"] forKey:@"ItemDescription"];//15
//                [newDevice setValue:[dicEstimator_Master valueForKey:@"isApprove"] forKey:@"isApprove"];//16
//
//                NSError *error = nil;
//                // Save the object to persistent store
//                if (![context save:&error]) {
//                    NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
//                }
//                else{
//                    NSLog(@"Saved Data");
//
//                }
//            }
//        }
//
//    }




@end
